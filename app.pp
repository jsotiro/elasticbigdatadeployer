include mysql::client

class { 'nodejs':
  manage_repo => true,
  version     => '0.10.26-1chl1~precise1',
}

## configurable parameters ##
$mysql_root_password = $::mysqlpass
$nodejs_app_location = '/var/app/current/ebd'
$update_code = $::appupdate

## install the required packages $$
package { 'build-essential':
  ensure => installed,
}

package { 'git':
  ensure => installed,
}

package { 'ruby1.9.3':
  ensure => installed,
}

package { 'python-pip':
  ensure => 'installed',
}

package { 'mrjob':
  ensure   => 'installed',
  provider => 'pip',
}

## make ruby 1.9.3 the default ##
exec { 'ruby1.9.3':
  command => '/usr/sbin/update-alternatives --install /usr/bin/ruby ruby /usr/bin/ruby1.9.3 400 --slave /usr/share/man/man1/ruby.1.gz ruby.1.gz /usr/share/man/man1/ruby1.9.3.1.gz --slave /usr/bin/ri ri /usr/bin/ri1.9.3 --slave /usr/bin/irb irb /usr/bin/irb1.9.3 --slave /usr/bin/rdoc rdoc /usr/bin/rdoc1.9.3',
  require => Package['ruby1.9.3'],
  unless  => '/usr/bin/ruby --version | grep 1.9.3',
}

##### NGINX #####
class { 'nginx': }

nginx::resource::upstream { 'app_elasticbigdata.com':
  members => [
    'localhost:3000',
  ],
  require => Exec['ruby1.9.3'],
}

nginx::resource::vhost { 'workspace.elasticbigdata.com':
  proxy            => 'http://app_elasticbigdata.com',
  proxy_set_header => ['X-Real-IP $remote_addr', 'X-Forwarded-For $proxy_add_x_forwarded_for', 'Host $http_host', 'X-NginX-Proxy true'],
  ensure           => present,
  listen_ip        => '*',
  listen_port      => '80',
  server_name      => ["$::ebdip", 'workspace.elasticbigdata.com', 'elasticbigdata.com'],
  require          => Exec['ruby1.9.3'],
}

nginx::resource::location { '/public':
  ensure         => 'present',
  location_alias => '/var/app/current/ebd',
  vhost          => 'workspace.elasticbigdata.com',
  require        => Exec['ruby1.9.3'],
}

## manage the application directory ##
file { ['/var/app', '/var/app/current']:
  ensure => 'directory',
  owner  => 'ubuntu',
  group  => 'ubuntu',
  mode   => '0755',
}

## manage the ubuntu user's ssh key ##
## this is tied to the bitbucket repo ##
file { '/home/ubuntu/.ssh':
  ensure => directory,
  mode   => '0700',
  owner  => 'ubuntu',
  group  => 'ubuntu',
}

file { '/home/ubuntu/.ssh/id_rsa':
  ensure  => 'present',
  mode    => '0600',
  owner   => 'ubuntu',
  group   => 'ubuntu',
  content => template('id_rsa'),
  require => File['/home/ubuntu/.ssh'],
}

file { '/home/ubuntu/.ssh/id_rsa.pub':
  ensure  => 'present',
  mode    => '0644',
  owner   => 'ubuntu',
  group   => 'ubuntu',
  content => template('id_rsa.pub'),
  require => File['/home/ubuntu/.ssh/id_rsa'],
}

file { 'ssh config':
  ensure  => 'present',
  content => "Host bitbucket.org\n\tStrictHostKeyChecking no\n\tIdentityFile ~/.ssh/id_rsa\n",
  path    => '/home/ubuntu/.ssh/config',
  owner   => 'ubuntu',
  group   => 'ubuntu',
  require => File['/home/ubuntu/.ssh/id_rsa.pub'],
}

## deploy the app via git clone ##
exec { 'app deployment':
  command => "/usr/bin/git clone -b $::apprevision git@bitbucket.org:jsotiro/elasticbigdata.git /var/app/current/ebd",
  user    => 'ubuntu',
  require => [ File[['/var/app', '/var/app/current']], File['ssh config'], Package['git']],
  unless  => '/usr/bin/test -d /var/app/current/ebd',
}

## modify the app config to populate mysql server and password ##
file { '/var/app/current/ebd/config/config.default.js':
  content => template('config.default.js'),
  require => Exec['app deployment'],
  owner   => 'ubuntu',
  group   => 'ubuntu',
  mode    => '0644',
}

## run the application ##
exec { '/var/app/current/ebd/node_modules/forever/bin/forever start server.js':
  cwd    => '/var/app/current/ebd',
  unless => '/bin/ps -ef | /bin/grep -v grep | /bin/grep nodejs',
  require => [ Nginx::Resource::Vhost['workspace.elasticbigdata.com'], Nginx::Resource::Location['/public'], Exec['app deployment'] ],
}

##### nodejs #####
# install npm packages

Package['nodejs'] -> Exec<| environment == 'foo=bar' |> -> Exec['/var/app/current/ebd/node_modules/forever/bin/forever start server.js']

exec { 'forever':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install forever',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/express',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'express':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install express',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/express',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'session':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install session',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/session',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'jade':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install jade',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/jade',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'passport':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install passport',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/passport',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'passport-local':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install passport-local',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/passport-local',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'connect-flash':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install connect-flash',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/connect-flash',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'password-hash':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install password-hash',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/password-hash',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'mysql':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install mysql',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/mysql',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'marked':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install marked',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/marked',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'nodemailer':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install nodemailer',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/nodemailer',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'recaptcha':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install recaptcha',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/recaptcha',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'socket.io':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install socket.io',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/socket.io',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'mocha':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install mocha',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/mocha',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'supertest':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install supertest',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/supertest',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'should':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install should',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/should',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'nock':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install nock',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/nock',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'expect.js':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install expect.js',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/expect.js',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

exec { 'passport.socketio':
  cwd         => '/var/app/current/ebd',
  command     => '/usr/bin/npm install passport.socketio',
  unless      => '/usr/bin/test -d /var/app/current/ebd/node_modules/passport.socketio',
  environment => 'foo=bar',
  require     => Exec['app deployment'],
}

