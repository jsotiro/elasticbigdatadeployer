include mysql::client

## configurable parameters ##
$mysql_root_password = $::mysqlpass

class { '::mysql::server':
  root_password => $mysql_root_password,
  manage_config_file => 'true',
}

class { '::mysql::bindings':
  python_enable => 'true',
  ruby_enable   => 'true',
}

mysql::db { 'collabjs':
  user     => 'collabjs',
  password => $mysql_root_password,
  sql      => '/home/ubuntu/dbload/data/schema/mysql/schema.mysql.sql',
  require  => Exec['app retrieve'],
}

mysql::db { 'elasticbigdata':
  user     => 'elasticbigdata',
  password => $mysql_root_password,
  sql      => '/home/ubuntu/dbload/data/schema/mysql/elasticbigdata.sql',
  require  => Exec['app retrieve'],
}

package { 'git':
  ensure => installed,
}

file { '/home/ubuntu/.ssh':
  ensure => directory,
  mode   => '0700',
  owner  => 'ubuntu',
  group  => 'ubuntu',
}

file { '/home/ubuntu/.ssh/id_rsa':
  ensure  => 'present',
  mode    => '0600',
  owner   => 'ubuntu',
  group   => 'ubuntu',
  content => template("id_rsa"),
  require => File['/home/ubuntu/.ssh'],
}

file { '/home/ubuntu/.ssh/id_rsa.pub':
  ensure  => 'present',
  mode    => '0644',
  owner   => 'ubuntu',
  group   => 'ubuntu',
  content => template("id_rsa.pub"),
  require => File['/home/ubuntu/.ssh'],
}

file { 'ssh config':
  ensure  => 'present',
  content => "Host bitbucket.org\n\tStrictHostKeyChecking no\n\tIdentityFile ~/.ssh/id_rsa\n",
  path    => '/home/ubuntu/.ssh/config',
  owner   => 'ubuntu',
  group   => 'ubuntu',
  require => File['/home/ubuntu/.ssh'],
}

exec { 'app retrieve':
  command => "/usr/bin/git clone -b $::apprevision git@bitbucket.org:jsotiro/elasticbigdata.git /home/ubuntu/dbload",
  user    => 'ubuntu',
  require => [ File['ssh config'], Package['git'] ],
  unless  => '/usr/bin/test -d /home/ubuntu/dbload',
}
