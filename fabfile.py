from fabric.api import *

def install():
  put('puppetlabs-release-precise.deb', \
      '/var/tmp/puppetlabs-release-precise.deb')
  sudo('dpkg -i /var/tmp/puppetlabs-release-precise.deb', warn_only=True)
  sudo('apt-get update')
  sudo('apt-get install -y puppet-common')
  sudo('for i in puppetlabs-apt puppetlabs-mysql jfryman-nginx \
        puppetlabs-nodejs; do puppet module install $i; done', \
        warn_only=True)

def mysql_server(mysql_pass, revision):
  with shell_env(FACTER_MYSQLPASS=mysql_pass, FACTER_APPREVISION=revision):
    put('mysql.pp', '/home/ubuntu/mysql.pp')
    put('templates/*', '/var/tmp')
    sudo('/usr/bin/puppet apply --templatedir=/var/tmp /home/ubuntu/mysql.pp')

def app_server(ebdip, mysql_server, mysql_pass, revision):
  with shell_env(FACTER_EBDIP=ebdip, FACTER_MYSQLSERVER=mysql_server, FACTER_MYSQLPASS=mysql_pass, \
                 FACTER_APPREVISION=revision):
    put('app.pp', '/home/ubuntu/app.pp')
    put('templates/*', '/var/tmp')
    sudo('/usr/bin/puppet apply --templatedir=/var/tmp /home/ubuntu/app.pp')

def update():
  sudo('cd /var/app/current/ebd && /usr/bin/git pull', user='ubuntu')
  sudo('killall nodejs')
  sudo('cd /var/app/current/ebd && forever start server.js')
